
#ifndef FILE_H
#define FILE_H

#include <stdbool.h>
#include <stdio.h>

typedef FILE *fbreader;
typedef FILE *fbwriter;

fbreader fbreader_open(char *filename);
fbwriter fbwriter_open(char *filename);

void file_skip(FILE *this, size_t offset);
void file_seek(FILE *this, size_t offset);
bool fbreader_read(fbreader this, void *buffer, size_t struct_size, size_t n, size_t padding);
bool fbwriter_write(fbwriter this, const void *buffer, size_t struct_size, size_t n, size_t padding);

#endif
