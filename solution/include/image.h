#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct pixel {
	uint8_t b;
	uint8_t g;
	uint8_t r;
} __attribute__((packed));

struct image {
	size_t width;
	size_t height;

	struct pixel *data;
};

enum {
	PIXEL_SIZE = sizeof(struct pixel)
};

struct image image_create(size_t width, size_t height);
struct pixel *image_at(const struct image *this, size_t col, size_t row);
void image_destroy(struct image *restrict this);

#endif
