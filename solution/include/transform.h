#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

struct image image_rotate_90deg(const struct image source);

#endif
