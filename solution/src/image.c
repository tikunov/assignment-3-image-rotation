#include "image.h"

#include <stdlib.h>

struct image image_create(size_t width, size_t height) {
	struct image this;

	size_t data_size = width * height;

	this.width = width;
	this.height = height;
	this.data = (struct pixel *) malloc(data_size * sizeof(struct pixel));

	return this;
}

struct pixel *image_at(const struct image *const this, size_t col, size_t row) {
	return this->data + col + row * this->width;
}

void image_destroy(struct image *restrict this) {
	free(this->data);
	this->data = NULL;
}
