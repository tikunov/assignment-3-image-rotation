#include "file.h"

#include <stdint.h>

fbreader fbreader_open(char *filename) {
	return fopen(filename, "rb");
}

fbwriter fbwriter_open(char *filename) {
	return fopen(filename, "wb");
}

void file_skip(FILE *this, size_t offset) {
	fseek(this, (long) offset, SEEK_CUR);
}

void file_seek(FILE *this, size_t offset) {
	fseek(this, (long) offset, SEEK_SET);
}

bool fbreader_read(fbreader this, void *buffer, size_t struct_size, size_t n, size_t padding) {
	bool status = fread(buffer, struct_size, n, this);
	file_skip(this, padding);

	return status;
}

bool fbwriter_write(fbwriter this, const void *buffer, size_t struct_size, size_t n, size_t padding) {
	bool write_status = fwrite(buffer, struct_size, n, this);
	uint8_t trash[3] = {0};
	fwrite(trash, 1, padding, this);

	return write_status;
}
