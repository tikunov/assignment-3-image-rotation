#include "bmp.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

enum {
	BMP_BF_TYPE = 19778,
	BMP_BF_RESERVED = 0,
	BMP_FILE_HEADER_SIZE = 14,
	BMP_BI_PLANES = 1,
	BMP_BI_BIT_COUNT = 24,
	BMP_BI_COMPRESSION = 0,
	BMP_BI_X_PELS_PER_METER = 0,
	BMP_BI_Y_PELS_PER_METER = 0,
	BMP_BI_CLR_USED = 0,
	BMP_BI_CLR_IMPORTANT = 0,
	BMP_HEADER_SIZE = sizeof(struct bmp_header)
};

static bool read_header(fbreader in, struct bmp_header *const header) {
	if (!fbreader_read(in, header, BMP_HEADER_SIZE, 1, 0)) {
		return false;
	}

	file_seek(in, header->bOffBits);

	return true;
}

static inline size_t size_t_divide_round_up(size_t n, size_t divisor) {
	return ((n + divisor - 1) / divisor);
}

static size_t get_padding(size_t row_size) {
	size_t padding = size_t_divide_round_up(row_size, 4) * 4 - row_size;
	return padding;
}

static bool read_data(fbreader in, const struct image *const img) {

	size_t byte_width = PIXEL_SIZE * (img->width);
	size_t padding = get_padding(byte_width);

	struct pixel *start = img->data;

	for (
		struct pixel *row = img->data;
		row - start < img->width * img->height;
		row += img->width
	) {
		fbreader_read(in, row, PIXEL_SIZE, img->width, padding);
	}

	return true;
}

enum read_status from_bmp(fbreader in, struct image *const img) {
	struct bmp_header header;
	bool read_header_ok = read_header(in, &header);

	if (!read_header_ok) {
		return READ_INVALID_HEADER;
	}

	*img = image_create(header.biWidth, header.biHeight);

	bool read_data_ok = read_data(in, img);
	if (!read_data_ok) {
		return READ_INVALID_BITS;
	}

	fclose(in);
	return READ_OK;
}

static struct bmp_header generate_header(const struct image *const img) {
	struct bmp_header header = (struct bmp_header) {0};

	size_t offset = BMP_HEADER_SIZE;
	size_t info_size = offset - BMP_FILE_HEADER_SIZE;
	size_t row_size = PIXEL_SIZE * img->width;
	size_t padded_row_size = row_size + get_padding(row_size);
	size_t image_size = padded_row_size * img->height;

	header.bfType = BMP_BF_TYPE;
	header.bfileSize = image_size + offset;
	header.bfReserved = BMP_BF_RESERVED;
	header.bOffBits = offset;
	header.biSize = info_size;
	header.biWidth = img->width;
	header.biHeight = img->height;
	header.biPlanes = BMP_BI_PLANES;
	header.biBitCount = BMP_BI_BIT_COUNT;
	header.biCompression = BMP_BI_COMPRESSION;
	header.biSizeImage = image_size;
	header.biXPelsPerMeter = BMP_BI_X_PELS_PER_METER;
	header.biYPelsPerMeter = BMP_BI_Y_PELS_PER_METER;
	header.biClrUsed = BMP_BI_CLR_USED;
	header.biClrImportant = BMP_BI_CLR_IMPORTANT;

	return header;
}

enum write_status to_bmp(fbwriter out, const struct image *const img) {
	struct bmp_header header = generate_header(img);
	size_t padding = get_padding(PIXEL_SIZE * (img->width));

	if (!fbwriter_write(out, &header, BMP_HEADER_SIZE, 1, 0)) {
		return WRITE_ERROR;
	}

	for (size_t row_number = 0; row_number < img->height; ++row_number) {
		const struct pixel *current_row = img->data + row_number * img->width;

		if (!fbwriter_write(out, current_row, PIXEL_SIZE, img->width, padding)) {
			return WRITE_ERROR;
		}
	}

	fclose(out);
	return WRITE_OK;
}
