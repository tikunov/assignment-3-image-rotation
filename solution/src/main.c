#include <stdio.h>

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "transform.h"

int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image>\n", argv[0]);
        return EXIT_FAILURE;
    }

    char *in_filename = argv[1];
    char *out_filename = argv[2];

    fbreader in = fbreader_open(in_filename);
    struct image img = (struct image) {0};

    switch (from_bmp(in, &img)) {
    case READ_OK:
        break;
    case READ_INVALID_HEADER:
        fprintf(stderr, "Couldn't read image header from '%s'\n", in_filename);
        return EXIT_FAILURE;
    case READ_INVALID_BITS:
        fprintf(stderr, "Couldn't read image data from '%s'\n", in_filename);
        return EXIT_FAILURE;
    }

    struct image rotated = image_rotate_90deg(img);
    image_destroy(&img);

    fbwriter out = fbwriter_open(out_filename);

    switch (to_bmp(out, &rotated)) {
    case WRITE_OK:
        break;
    case WRITE_ERROR:
        fprintf(stderr, "Couldn't save transformed image to '%s'\n", out_filename);
        return EXIT_FAILURE;
    }
    image_destroy(&rotated);

    return EXIT_SUCCESS;
}
